<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Prototype</title>
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>
    @include('inc.navbar')
    <br>
    <div class="container">
      @include('inc.messages')
      @yield('content')
    </div>
  </body>
</html>
