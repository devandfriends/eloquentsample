<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();

      $levels = DB::table('user_levels')->pluck('id');
      $status = DB::table('employee_statuses')->pluck('id');
      foreach(range(1,10) as $index){
        DB::table('users')->insert([
          'name' => $faker->name,
          'employee_status_id' => $faker->randomElement($status) ,
          'user_level_id' => $faker->randomElement($levels),
          'email' => $faker->email,
          'password' => bcrypt('secret'),
          'created_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        ]);
      }
    }
}
